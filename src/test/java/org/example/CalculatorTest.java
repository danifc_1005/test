package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

class CalculatorTest extends Calculator {

    Calculator c = new Calculator();

    @Test
    void testAdd() {
        assertEquals(10,c.add(5,5));
        assertEquals(8,c.add(-1,9));
        assertEquals(0,c.add(-2525,2525));
        assertEquals(-5678,c.add(0,-5678));
        assertEquals(0,c.add(0,0));
    }

    @Test
    void testSubtract() {
        assertEquals(30, c.subtract(34,4));
        assertEquals(-1, c.subtract(13,14));
        assertEquals(0, c.subtract(0,0));
    }

    @Test
    void testMultiply() {
        assertEquals(25, c.multiply(5,5));
        assertEquals(-1, c.multiply(1,-1));
    }

    @Test
    void testSqrt() {
        assertEquals(9, c.sqrt(81), 0.01);
        assertEquals(3, c.sqrt(9), 0.01);
    }

    @Test
    void testFact() {
        assertEquals(120, c.fact(5));
    }
}